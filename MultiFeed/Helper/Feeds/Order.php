<?php


namespace Hunters\MultiFeed\Helper\Feeds;

class Order extends \Magento\Framework\App\Helper\AbstractHelper
{

    const COMPLETE_ORDER_STATUS = 'complete';
    protected $collectionFactory;
    protected $csv;

    protected $_orderItemCollection;
    protected $_orderItem;


    protected $_order;
    protected $_shipping;
    protected $_item;
    protected $_micrositeDetail;
    protected $_orderAddress;
    protected $_resourceConnection;


    public function __construct(
        \Toppik\Sap\Model\ResourceModel\OrderItem\CollectionFactory $collectionFactory,
        \Hunters\MultiFeed\Helper\Csv $csv,
        \Toppik\Sap\Model\ResourceModel\OrderItem\CollectionFactory  $orderItemCollection,
        \Toppik\Sap\Model\OrderItem $orderItem,
        \Magento\Sales\Model\Order $order,
        \Magento\Sales\Model\Order\Shipment $shipment,
        \Magento\Sales\Model\Order\Item $item,
        \Toppik\Microsite\Model\Order\Detail $micrositeDetail,
        \Magento\Framework\App\ResourceConnection $resourceConnection

    ) {
        $this->collectionFactory = $collectionFactory;
        $this->csv = $csv;
        $this->_orderItemCollection =$orderItemCollection;
        $this->_orderItem = $orderItem;

        $this->_order = $order;
        $this->_shipping = $shipment;
        $this->_item = $item;
        $this->_micrositeDetail = $micrositeDetail;
        $this->_resourceConnection = $resourceConnection;

    }

    private function _getConnection() {
        static $connection;
        if (empty($connection)) {
            $connection = $this->_resourceConnection->getConnection();
        }
        return $connection;
    }

    public function getProductSkuById($productId) {
        return $this->_productRepository->getById($productId)->getSku();
    }

    public function getParenSku($orderId, $productId) {
        $sql = "select if (sap.item_sku = i.sku, ' ', i.sku) as res from sap_order_queue_items as sap ".
            "join sales_order_item as i on sap.item_id = i.item_id ".
            " where sap.order_id = :orderId and sap.item_material_number = :productId";
        $bind = [
            'orderId' => $orderId,
            'productId' => $productId,
        ];
        return $this->_getConnection()->fetchOne($sql, $bind);
    }

    public function _checkOrderInSubsProfileOrderss($orderId) {
        $sql = "select profile_id from subscriptions_profiles_orders where order_id = :orderId";
        $bind = [
          'orderId' => $orderId
        ];
        return count($this->_getConnection()->fetchAll($sql, $bind)) == 0 ? true : false ;
    }

    public function _checkOrderInSalesCreditmemo($orderId) {
        $sql = "select entity_id from sales_creditmemo where order_id = :orderId";
        $bind = [
            'orderId' => $orderId
        ];
        return count($this->_getConnection()->fetchAll($sql, $bind)) == 0 ? true : false ;
    }

    public function _getOrderDiscount($orderId) {
        $collection = $this->_orderItemCollection->create();
        $collection->getSelect()
            ->where(new \Zend_Db_Expr("order_id = $orderId"))
            ->columns([
                'discount' => 'item_discount'
            ]);
        $discount = 0.00;
        foreach ($collection as $item) {
            $discount += floatval($item->getDiscount());
        }
        return $discount;
    }

    public function _getQryShipped($orderId) {
        $collection = $this->_orderItemCollection->create();
        $collection->getSelect()
            ->where(new \Zend_Db_Expr("order_id = $orderId"))
            ->columns([
                'qty' => 'item_qty'
            ]);
        $qty = [];
        foreach ($collection as $item) {
            $qty[] = intval($item->getQty());
        }
        return $qty;
    }

    public function _getItemsPrice($orderId) {
        $collection = $this->_orderItemCollection->create();
        $collection->getSelect()
            ->where(new \Zend_Db_Expr("order_id = $orderId"))
            ->columns([
                'item_price' => 'item_price'
            ]);
        $itemsPrice = [];
        foreach ($collection as $item) {
            $itemsPrice[] = floatval($item->getItemPrice());
        }
        return $itemsPrice;
    }

    public function _getItemsTax($orderId) {
        $collection = $this->_orderItemCollection->create();
        $collection->getSelect()
            ->where(new \Zend_Db_Expr("order_id = $orderId"))
            ->columns([
                'item_tax' => 'item_tax'
            ]);
        $itemsTax = [];
        foreach ($collection as $item) {
            $itemsTax[] = floatval($item->getItemTax());
        }
        return $itemsTax;
    }

    public function _getPsnValue($orderId) {
        $collection = $this->_orderItemCollection->create();
        $collection->getSelect()
            ->where(new \Zend_Db_Expr("order_id = $orderId"))
            ->columns([
                'item_material_number' => 'item_material_number'
            ]);
        $itemMaterialNumber = [];
        foreach ($collection as $item) {
            $itemMaterialNumber[] = $item->getItemMaterialNumber();
        }
        return $itemMaterialNumber;
    }

    public function _getChildSku($orderId) {
        $collection = $this->_orderItemCollection->create();
        $collection->getSelect()
            ->where(new \Zend_Db_Expr("order_id = $orderId"))
            ->columns([
                'item_sku' => 'item_sku',
                'item_id' => 'item_id'
            ]);
        $itemSku = [];
        foreach ($collection as $item) {
            $itemSku[$item->getItemId()] = $item->getItemSku();
        }
        return $itemSku;
    }

    public function getEntityId(\Magento\Sales\Model\Order $order) {
        return $order->getEntityId();
    }

    public function getIncrementId(\Magento\Sales\Model\Order $order) {
        return $order->getIncrementId();
    }

    public function getCreateDate(\Magento\Sales\Model\Order $order) {
        return $order->getCreatedAt();
    }

    public function getShippingData(\Magento\Sales\Model\Order\Shipment $shipment) {
        return $shipment->getCreatedAt();
    }

    public function getFreight(\Magento\Sales\Model\Order $order) {
        return $order->getShippingAmount() + $order->getShippingTaxAmount();
    }

    public function getOrderSource(\Magento\Sales\Model\Order $order) {
        return $order->getData('merchant_source');
    }

    public function getOrderGrandTotal(\Magento\Sales\Model\Order $order) {
        return $order->getGrandTotal();
    }

    public function getQtyOrder(\Magento\Sales\Model\Order\Item $item) {
        return $item->getQtyOrdered();
    }

    public function getPromoCode(\Toppik\Microsite\Model\Order\Detail $orderDetail) {
        return $orderDetail->getData('media_code');
    }

    public function getProjectCode(\Toppik\Microsite\Model\Order\Detail $orderDetail) {
        return $orderDetail->getData('project_code');
    }

    public function getShippingAddress(\Magento\Sales\Model\Order $order) {
        return $order->getShippingAddress();
    }

    public function getBillingAddress(\Magento\Sales\Model\Order $order) {
        return $order->getBillingAddress();
    }

    public function getOrderStatus(\Magento\Sales\Model\Order $order) {
        return $order->getStatus();
    }

    public function getCustomerEmail(\Magento\Sales\Model\Order $order) {
        return $order->getCustomerEmail();
    }

    public function getRefund(\Magento\Sales\Model\Order $order) {
        return $order->getTotalRefunded();
    }

    public function generatorOrderData($itemPrice, $itemTax, $psnValue, $qtyShipped) {
        for ($i = 0; $i < count($itemPrice); $i++) {
            yield [
                'itemPrice' => $itemPrice[$i],
                'itemTax' =>$itemTax[$i],
                'itemMaterialNumber' => $psnValue[$i],
                'qtyShipped' => $qtyShipped[$i]
            ];
        }
    }

    public function getCustomerPrefix(\Magento\Sales\Model\Order $order) {
        return $order->getCustomerPrefix();
    }

    public function generateCompleteOrders($filterIds, $storeId = null)
    {

        $data = [];
        $data[] = $this->getHeaders();
        sort($filterIds);
        foreach ($filterIds as $id) {
            $order = $this->_order->load($id);
            if ($this->_checkOrderInSubsProfileOrderss($id)
                && $this->_checkOrderInSalesCreditmemo($id)
                && $order->getStatus() == self::COMPLETE_ORDER_STATUS) {

                $shipment = $this->_shipping->loadByIncrementId($order->getIncrementId());
                $item = $this->_item->load($order->getEntityId(), 'order_id');
                $orderDetail = $this->_micrositeDetail->load($order->getEntityId(), 'order_id');

                $tax = $this->_getItemsTax($this->getEntityId($order));
                $itemPrice = $this->_getItemsPrice($this->getEntityId($order));
                $psnValue = $this->_getPsnValue($this->getEntityId($order));
                $qtyShipped = $this->_getQryShipped($this->getEntityId($order));

                $orderMainData = $this->generatorOrderData($itemPrice, $tax, $psnValue, $qtyShipped);
                $orders = [];
                while ($orderMainData->valid()) {
                    $skipRow = '';
                    $freight = '';
		    $discount = '';
		    $refund = '';
                    if (!in_array($this->getEntityId($order), $orders)) {
			$discount = abs($this->_getOrderDiscount($this->getEntityId($order)));
			$refund = floatval($this->getRefund($order));
                        $skipRow = 1;
                        $freight = $this->getFreight($order);
                        $orders[] = $this->getEntityId($order);
                    }
                    $data[] = [
                        $this->getIncrementId($order),
                        $this->convertDate($this->getCreateDate($order)),
                        $this->convertDate($this->getShippingData($shipment)),
                        $freight,
                        $this->getOrderSource($order),
                        $this->getParenSku($this->getEntityId($order), $orderMainData->current()['itemMaterialNumber']),
                        $this->getOrderGrandTotal($order),
                        $orderMainData->current()['itemMaterialNumber'],
                        intval($this->getQtyOrder($item)),
			$orderMainData->current()['itemPrice'],
                        $orderMainData->current()['itemPrice'],
			intval($this->getQtyOrder($item)) * floatval($orderMainData->current()['itemPrice']),
                        'Y',
                        $this->getPromoCode($orderDetail),
                        $this->getProjectCode($orderDetail),
                        '0',
                        $orderMainData->current()['itemTax'],
                        $this->getShippingAddress($order)->getFirstname(),
                        $this->getShippingAddress($order)->getLastname(),
                        strval($this->getShippingAddress($order)->getData("street")),
                        '',
                        $this->getShippingAddress($order)->getCity(),
                        $this->getShippingAddress($order)->getRegion(),
                        $this->getShippingAddress($order)->getPostcode(),
                        $this->getShippingAddress($order)->getTelephone(),
                        $this->getShippingAddress($order)->getCountryId(),
                        $this->getBillingAddress($order)->getFirstname(),
                        $this->getBillingAddress($order)->getLastname(),
                        strval($this->getBillingAddress($order)->getData("street")),
                        '',
                        $this->getBillingAddress($order)->getCity(),
                        $this->getBillingAddress($order)->getRegion(),
                        $this->getBillingAddress($order)->getPostcode(),
                        '',
                        $this->getOrderStatus($order),
                        $this->getCustomerEmail($order),
                        '',
                        $skipRow,
			$discount,
			$refund,
                        $this->getCustomerPrefix($order)
                    ];
                    $orderMainData->next();
                }
            }
        }
        return $this->csv->generate('multifeed_order_complete', $data, $storeId);
    }

    public function generateReturnOrders($filterIds, $storeId = null)
    {
      $collection = $this->collectionFactory
        ->create();

      $ids = implode(',', $filterIds);
      $collection->getSelect()
        ->where(new \Zend_Db_Expr("main_table.order_id in ($ids)"));

      $collection->getSelect()
        ->where(new \Zend_Db_Expr('o.total_paid - o.total_refunded = 0'));

      $collection->getSelect()->join(
              ['o' => 'sales_order'],
              new \Zend_Db_Expr('o.entity_id = main_table.order_id and o.status = "closed"'),
              []
          );

      $collection->getSelect()->joinLeft(
              ['s' => 'sales_shipment'],
              's.order_id = main_table.order_id',
              []
          );

      $scmi = new \Zend_Db_Expr('(select increment_id,order_id,count(*) as memo_qty from sales_creditmemo group by order_id having memo_qty = 1)');
      $collection->getSelect()->join(
              ['scmi' => $scmi],
              'scmi.order_id = main_table.order_id',
              []
      );

      $collection->getSelect()->joinLeft(
              ['i' => 'sales_order_item'],
              'i.item_id = main_table.item_id',
              []
          );

      $collection->getSelect()->joinLeft(
              ['mod' => 'microsite_order_detail'],
              'mod.order_id = o.entity_id',
              []
          );

      $collection->getSelect()->joinLeft(
                ['shipping' => 'sales_order_address'],
                new \Zend_Db_Expr('shipping.parent_id = main_table.order_id and shipping.address_type = "shipping"'),
                []
            );

      $collection->getSelect()->joinLeft(
                ['billing' => 'sales_order_address'],
                new \Zend_Db_Expr('billing.parent_id = main_table.order_id and billing.address_type = "billing"'),
                []
            );



      $profiles = new \Zend_Db_Expr('(select sp.frequency_length,spo.*, if (b.child_id is not null, b.child_id, sp.sku) as sku from subscriptions_profiles_orders as spo left join subscriptions_profiles as sp on sp.profile_id = spo.profile_id left join sap_sku_breakdown as b on b.parent_id = sp.sku)');


      $collection->getSelect()->joinLeft(
        ['profile' => $profiles],
        'profile.order_id = main_table.order_id and profile.sku = main_table.item_sku',
        []
      );

      // get media code from parent order
      $parentOrderByProfile = new \Zend_Db_Expr('(select profile_id, min(order_id) as parentOrderId from subscriptions_profiles_orders as spo2 group by profile_id )');
      $collection->getSelect()->joinLeft(
        ['parentOrder' => $parentOrderByProfile],
        'profile.profile_id = parentOrder.profile_id',
        []
      );
      $collection->getSelect()->joinLeft(
              ['mod2' => 'microsite_order_detail'],
              'mod2.order_id = parentOrder.parentOrderId',
              []
          );

          $collection
              ->getSelect()
              ->group(new \Zend_Db_Expr('main_table.order_id,i.sku,main_table.item_sku,main_table.item_price,profile.frequency_length'))
              ->order('o.increment_id DESC');

      $collection->getSelect()
            ->reset(\Zend_Db_Select::COLUMNS)
            ->columns([
                'entity_id' => 'o.entity_id',
                'increment_id' => 'o.increment_id',
                'created_at' => 'o.created_at',
                'coupon_code' => 'o.coupon_code',
                'source' => 'o.source',
                'grand_total' => 'o.grand_total',
                'shipping_amount' => 'o.shipping_amount',
                'shipping_tax_amount' => 'o.shipping_tax_amount',
                'customer_email' => 'o.customer_email',
                'ship_canceled_date' => 's.created_at',
                'media_code' => 'mod.media_code',
                'project_code' => 'mod.project_code',
                'parent_media_code' => 'mod2.media_code',
                'parent_project_code' => 'mod2.project_code',
                'psn_value' => 'main_table.item_material_number',
                'qty_ordered' => 'i.qty_ordered',
                // 'qty_shipped' => 'main_table.item_qty',
                'qty_shipped' => new \Zend_Db_Expr('SUM(main_table.item_qty)'),
                'parent_sku' => 'i.sku',
                'child_sku' => 'main_table.item_sku',
                'item_price' => new \Zend_Db_Expr('IF (main_table.item_price is not null, main_table.item_price, 0)'),
                'order_status' => new \Zend_Db_Expr('IF(s.order_id is not null and o.status = "closed","return", o.status)'),
                'item_tax' => 'main_table.item_tax',
                'child_sku' => 'main_table.item_sku',
                'profile_id' => 'profile.profile_id',
                'item_creditmemo_id' => 'scmi.increment_id',
                'sfirstname' => 'shipping.firstname',
                'slastname' => 'shipping.lastname',
                'sregion' => 'shipping.region',
                'spostcode' => 'shipping.postcode',
                'stelephone' => 'shipping.telephone',
                'scountry_id' => 'shipping.country_id',
                'scity' => 'shipping.city',
                'sstreet' => 'shipping.street',
                'bfirstname' => 'billing.firstname',
                'blastname' => 'billing.lastname',
                'bregion' => 'billing.region',
                'bpostcode' => 'billing.postcode',
                'btelephone' => 'billing.telephone',
                'bcountry_id' => 'billing.country_id',
                'bcity' => 'billing.city',
                'bstreet' => 'billing.street',
                'refund' => 'o.total_refunded',
		'prefix' => 'shipping.prefix'
            ]);

            $skuMaxPrice = [];
            // add array of orders with profile_id
            foreach ($collection as $item) {
              if ($item->getProfileId()) {
                $ordersWithProfiles[] = $item->getEntityId();

                //remove profile_id from not subscription item
                    $skuMaxPrice[$item->getEntityId()][$item->getPsnValue()] = $skuMaxPrice[$item->getEntityId()][$item->getPsnValue()] ?? [];
                    if (!in_array($item->getItemPrice(), $skuMaxPrice[$item->getEntityId()][$item->getPsnValue()])) {
                      $skuMaxPrice[$item->getEntityId()][$item->getPsnValue()][] = $item->getItemPrice();
                    }

                }
            }

            $orders = [];
            $data[] = $this->getHeaders();

            foreach ($collection as $item) {

              $skipRow = '';
              $freight = '';
	      $discount = '';
	      $refund = '';
              if (!in_array($item->getEntityId(), $orders)) {
		$discount = abs($this->_getOrderDiscount($item->getEntityId()));
		$refund = $item->getRefund();
                $skipRow = 1;
                $freight = $item->getShippingAmount() + $item->getShippingTaxAmount();
                $orders[] = $item->getEntityId();
              }

              $profileId = $item->getProfileId();
              if (isset($skuMaxPrice[$item->getEntityId()][$item->getPsnValue()]) &&
                count($skuMaxPrice[$item->getEntityId()][$item->getPsnValue()]) > 1 &&
                max($skuMaxPrice[$item->getEntityId()][$item->getPsnValue()]) == $item->getItemPrice()
              ){
                $profileId = '';
              }

              $data[] = [
                $item->getIncrementId(),
                $this->convertDate($item->getCreatedAt()),
                $this->convertDate($item->getShipCanceledDate()),
                $freight,
                $item->getSource(),
                $this->getParenSku($item->getEntityId(), $item->getPsnValue()),
                $item->getGrandTotal(),
                $item->getPsnValue(),
		intval($item->getQtyShipped()),
                intval($item->getQtyShipped()),
                $item->getItemPrice(),
		intval($item->getQtyOrdered()) * floatval($item->getItemPrice()),
                'Y',
                $item->getMediaCode() ?? $item->getParentMediaCode(),
                $item->getProjectCode() ?? $item->getParentProjectCode(),
                0,
                $item->getItemTax(),
                $item->getSfirstname(),
                $item->getSlastname(),
                $item->getSstreet(),
                '',
                $item->getScity(),
                $item->getSregion(),
                $item->getSpostcode(),
                $item->getStelephone(),
                $item->getScountryId(),

                $item->getBfirstname(),
                $item->getBlastname(),
                $item->getBstreet(),
                '',
                $item->getBcity(),
                $item->getBregion(),
                $item->getBpostcode(),
                // $item->getBtelephone(),
                // $item->getBcountryId(),
                $item->getItemCreditmemoId(),
                $item->getOrderStatus(),
                $item->getCustomerEmail(),
                $profileId,
                $skipRow,
		$discount,
		$refund,
                $item->getPrefix()
              ];

            }

            return $this->csv->generate('multifeed_order_return', $data, $storeId);
    }

    public function generateSubsOrders($filterIds, $storeId = null)
    {

      $collection = $this->collectionFactory
        ->create();

      $ids = implode(',', $filterIds);
      $collection->getSelect()
        ->where(new \Zend_Db_Expr("main_table.order_id in ($ids)"));

      // $collection->getSelect()
      //   ->where(new \Zend_Db_Expr('main_table.order_id in (select order_id from subscriptions_profiles_orders)'));
      $collection->getSelect()
        ->where(new \Zend_Db_Expr('main_table.order_id not in (select order_id from sales_creditmemo)'));

      $collection->getSelect()->join(
              ['o' => 'sales_order'],
              new \Zend_Db_Expr('o.entity_id = main_table.order_id and o.status = "complete"'),
              []
          );

      $collection->getSelect()->joinLeft(
              ['s' => 'sales_shipment'],
              's.order_id = main_table.order_id',
              []
          );
      $profiles = new \Zend_Db_Expr('(select sp.frequency_length, spo.*, if (b.child_id is not null, b.child_id, sp.sku) as sku from subscriptions_profiles_orders as spo left join subscriptions_profiles as sp on sp.profile_id = spo.profile_id left join sap_sku_breakdown as b on b.parent_id = sp.sku)');


      $collection->getSelect()->joinLeft(
        ['profile' => $profiles],
        'profile.order_id = main_table.order_id and profile.sku = main_table.item_sku',
        []
      );

      // get media code from parent order
      $parentOrderByProfile = new \Zend_Db_Expr('(select profile_id, min(order_id) as parentOrderId from subscriptions_profiles_orders as spo2 group by profile_id )');
      $collection->getSelect()->joinLeft(
        ['parentOrder' => $parentOrderByProfile],
        'profile.profile_id = parentOrder.profile_id',
        []
      );
      $collection->getSelect()->joinLeft(
              ['mod2' => 'microsite_order_detail'],
              'mod2.order_id = parentOrder.parentOrderId',
              []
          );


      $collection->getSelect()->joinLeft(
              ['i' => 'sales_order_item'],
              'i.item_id = main_table.item_id',
              []
          );

      $collection->getSelect()->joinLeft(
              ['mod' => 'microsite_order_detail'],
              'mod.order_id = o.entity_id',
              []
          );

      $collection->getSelect()->joinLeft(
                ['shipping' => 'sales_order_address'],
                new \Zend_Db_Expr('shipping.parent_id = main_table.order_id and shipping.address_type = "shipping"'),
                []
            );

      $collection->getSelect()->joinLeft(
                ['billing' => 'sales_order_address'],
                new \Zend_Db_Expr('billing.parent_id = main_table.order_id and billing.address_type = "billing"'),
                []
            );

      $collection
          ->getSelect()
          ->group(new \Zend_Db_Expr('main_table.order_id,i.sku,main_table.item_sku,main_table.item_price,profile.frequency_length'))
          ->order('o.increment_id DESC');

      $collection->getSelect()
            ->reset(\Zend_Db_Select::COLUMNS)
            ->columns([
                'entity_id' => 'o.entity_id',
                'increment_id' => 'o.increment_id',
                'created_at' => 'o.created_at',
                'coupon_code' => 'o.coupon_code',
                'source' => 'o.source',
                'grand_total' => 'o.grand_total',
                'shipping_amount' => 'o.shipping_amount',
                'shipping_tax_amount' => 'o.shipping_tax_amount',
                'customer_email' => 'o.customer_email',
                'ship_canceled_date' => 's.created_at',
                'media_code' => 'mod.media_code',
                'project_code' => 'mod.project_code',
                'parent_media_code' => 'mod2.media_code',
                'parent_project_code' => 'mod2.project_code',
                'psn_value' => 'main_table.item_material_number',
                'qty_ordered' => 'i.qty_ordered',
                // 'qty_shipped' => 'main_table.item_qty',
                'qty_shipped' => new \Zend_Db_Expr('SUM(main_table.item_qty)'),
                'parent_sku' => 'i.sku',
                'child_sku' => 'main_table.item_sku',
                'item_price' => new \Zend_Db_Expr('IF (main_table.item_price is not null, main_table.item_price, 0)'),
                'order_status' => new \Zend_Db_Expr('IF(s.order_id is not null and o.status = "closed","return", o.status)'),
                'item_tax' => 'main_table.item_tax',
                'child_sku' => 'main_table.item_sku',
                'profile_id' => 'profile.profile_id',
                // 'item_creditmemo_id' => 'scmi.increment_id',
                'sfirstname' => 'shipping.firstname',
                'slastname' => 'shipping.lastname',
                'sregion' => 'shipping.region',
                'spostcode' => 'shipping.postcode',
                'stelephone' => 'shipping.telephone',
                'scountry_id' => 'shipping.country_id',
                'scity' => 'shipping.city',
                'sstreet' => 'shipping.street',
                'bfirstname' => 'billing.firstname',
                'blastname' => 'billing.lastname',
                'bregion' => 'billing.region',
                'bpostcode' => 'billing.postcode',
                'btelephone' => 'billing.telephone',
                'bcountry_id' => 'billing.country_id',
                'bcity' => 'billing.city',
                'bstreet' => 'billing.street',
                'refund' => 'o.total_refunded',
		'prefix' => 'shipping.prefix'
            ]);

            $orders = [];
            $data[] = $this->getHeaders();

            $ordersWithProfiles = [];
            $skuMaxPrice = [];
            // add array of orders with profile_id
            foreach ($collection as $item) {
              if ($item->getProfileId()) {
                $ordersWithProfiles[] = $item->getEntityId();

                //remove profile_id from not subscription item
                    $skuMaxPrice[$item->getEntityId()][$item->getPsnValue()] = $skuMaxPrice[$item->getEntityId()][$item->getPsnValue()] ?? [];
                    if (!in_array($item->getItemPrice(), $skuMaxPrice[$item->getEntityId()][$item->getPsnValue()])) {
                      $skuMaxPrice[$item->getEntityId()][$item->getPsnValue()][] = $item->getItemPrice();
                    }

                }
            }

            foreach ($collection as $item) {

              if (!in_array($item->getEntityId(), $ordersWithProfiles)) continue;

              $skipRow = '';
              $freight = '';
	      $discount = '';
	      $refund = '';
              if (!in_array($item->getEntityId(), $orders)) {
                $skipRow = 1;
		$discount = abs($this->_getOrderDiscount($item->getEntityId()));
		$refund = $item->getRefund();
                $freight = $item->getShippingAmount() + $item->getShippingTaxAmount();
                $orders[] = $item->getEntityId();
              }

              $profileId = $item->getProfileId();

              if (isset($skuMaxPrice[$item->getEntityId()][$item->getPsnValue()]) &&
                count($skuMaxPrice[$item->getEntityId()][$item->getPsnValue()]) > 1 &&
                max($skuMaxPrice[$item->getEntityId()][$item->getPsnValue()]) == $item->getItemPrice()
              ){
                $profileId = '';
              }

              $data[] = [
                $item->getIncrementId(),
                $this->convertDate($item->getCreatedAt()),
                $this->convertDate($item->getShipCanceledDate()),
                $freight,
                $item->getSource(),
                $this->getParenSku($item->getEntityId(), $item->getPsnValue()),
                $item->getGrandTotal(),
                $item->getPsnValue(),
		intval($item->getQtyShipped()),
                intval($item->getQtyShipped()),
                $item->getItemPrice(),
		intval($item->getQtyOrdered()) * floatval($item->getItemPrice()),
                'Y',
                $item->getMediaCode() ?? $item->getParentMediaCode(),
                $item->getProjectCode() ?? $item->getParentProjectCode(),
                0,
                $item->getItemTax(),
                $item->getSfirstname(),
                $item->getSlastname(),
                $item->getSstreet(),
                '',
                $item->getScity(),
                $item->getSregion(),
                $item->getSpostcode(),
                $item->getStelephone(),
                $item->getScountryId(),

                $item->getBfirstname(),
                $item->getBlastname(),
                $item->getBstreet(),
                '',
                $item->getBcity(),
                $item->getBregion(),
                $item->getBpostcode(),
                // $item->getBtelephone(),
                // $item->getBcountryId(),
                // $item->getItemCreditmemoId(),
                '',
                $item->getOrderStatus(),
                $item->getCustomerEmail(),
                $profileId,
                $skipRow,
		$discount,
		$refund,
                $item->getPrefix()
              ];

            }

            return $this->csv->generate('multifeed_order_subs', $data, $storeId);

    }

    public function renderHtmlTable(array $headers, array $data)
    {
      $html = '<table><tr>';

      foreach ($headers as $header) {
        $html.='<th>' . $header . '</th>';
      }

      $html.='</tr>';

      foreach ($data as $row) {
        $html.='<tr>';
        foreach ($row as $field) {
          $html.='<td>' . $field . '</td>';
        }
        $html.='</tr>';
      }

      $html.='</table>';
      $html.='<style>
        table, th, td {
          border: 1px solid black;
          border-collapse: collapse;
        }
        th, td {
          padding: 5px;
          text-align: left;
        }
        </style>';

      echo $html;
    }

    public function getHeaders()
    {
        return [
          "Order Number",
          "Order Received",
          "Ship/Canceled Date",
          "Freight",
          "Order Source",
          "Offer Code",
          "Order Total",
          "Item #",
          "Quantity Ordered",
          "Quantity Shipped",
          "Unit Price",
	  "Item Sum",
          "Taxable Y/N",
          "Promo Code",
          "Campaign of Promo Code",
          "Shipping Amount",
          "Sales Tax",
          "Ship to FName","Ship to LName","Ship to Addr1","Ship to Addr2","Ship to City","Ship to State","Ship to Zip","Ship to Phone","Ship to Country",
          "Bill to FName","Bill to LName","Bill to Addr1","Bill to Addr2","Bill to City","Bill to St","Bill to Zip",
          "Credit Memo Number",
          "Status",
          "Email",
          "Profile ID",
          "Skip Row",
          "Discount",
          "Refund",
	  "Prefix"
      ];
    }


    public function convertDate($date, $format = 'Y-m-d')
    {
      return $date ? date($format, strtotime($date)) : '';
    }
}
