<?php
namespace Hunters\MultiFeed\Helper;

class Queue extends \Magento\Framework\App\Helper\AbstractHelper
{
    const STATUS_NOT_SENT = 0;
    const STATUS_SENT = 1;

    protected $scopeConfig;
    protected $queueFactory;
    protected $subsHelper;
    protected $orderHelper;
    protected $cancelOrderHelper;
    protected $resource;
    protected $updateProjectMediaHelper;


    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Hunters\MultiFeed\Model\QueueFactory $queueFactory,
        \Hunters\MultiFeed\Helper\Feeds\Subs $subsHelper,
        \Hunters\MultiFeed\Helper\Feeds\Order $orderHelper,
        \Hunters\MultiFeed\Helper\Feeds\CancelOrder $cancelOrderHelper,
        \Hunters\MultiFeed\Helper\UpdateProjectMedia $updateProjectMediaHelper,
        \Magento\Framework\App\ResourceConnection $resource
    )
    {
        $this->queueFactory = $queueFactory;
        $this->subsHelper = $subsHelper;
        $this->orderHelper = $orderHelper;
        $this->cancelOrderHelper = $cancelOrderHelper;
        $this->resource = $resource;
        $this->updateProjectMediaHelper = $updateProjectMediaHelper;


    }

    public function add($entity_id, $entity_type, $store_id = null)
    {
      $queue = $this->queueFactory->create();
      $queue->setData('entity_id', $entity_id);
      $queue->setData('entity_type', $entity_type);
      $queue->setData('status', self::STATUS_NOT_SENT);
      $queue->setData('store_id', $store_id);
      $queue->save();
    }

    public function generateFeeds()
    {
      $_ids = $this->getFilterIds();

      $files = [];
      foreach ($_ids as $storeId => $types) {
        foreach ($types as $type => $ids) {
            $this->updateProjectMediaHelper->update($ids);
            switch ($type) {
              case 'active_subs':
              case 'cancelled_subs':
              case 'suspended_subs':
                $files = array_merge(
                  $this->subsHelper->generate($ids, $storeId),
                  $files
                );
                break;
              case 'complete_orders':
                $files[] = $this->orderHelper->generateCompleteOrders($ids, $storeId);
                break;
              case 'closed_orders':
                $files[] = $this->orderHelper->generateReturnOrders($ids, $storeId);
                break;
              case 'canceled_orders':
                $files[] = $this->cancelOrderHelper->generate($ids, $storeId);
                break;
              case 'subs_orders':
                $files[] = $this->orderHelper->generateSubsOrders($ids, $storeId);
                break;
            }

            $this->updateQueue($ids);

        }
      }
      return $files;

    }

    public function getFilterIds()
    {
      $queue = $this->queueFactory->create();
      $collection = $queue
        ->getCollection()
        ->addFieldToFilter('status', self::STATUS_NOT_SENT);

      $idsToChange = [];

      foreach ($collection as $item) {
        $storeId = $item->getStoreId();
        $type = $item->getEntityType();
        $entity_id = (int)$item->getEntityId();

        // get array with type -> [ids:int]
        isset($idsToChange[$storeId][$type]) ?
        (in_array($entity_id, $idsToChange[$storeId][$type])
          ?: array_push($idsToChange[$storeId][$type], $entity_id)
        ) : ($idsToChange[$storeId][$type] = [$entity_id]);
      }

      return $idsToChange;
    }

    public function updateQueue($ids) {
  		if(is_array($ids) && count($ids)) {
        $connection = $this->resource
          ->getConnection(
            \Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION
          );

        $sql = sprintf(
            'UPDATE %s SET %s WHERE %s',
            $this->resource->getTableName('hunters_multifeed_queue'),
            $connection->quoteInto('status = ?', self::STATUS_SENT),
            $connection->quoteInto('entity_id IN (?)', $ids)
        );

        $connection->query($sql);

      }
    }

}
