<?php

namespace Hunters\MultiFeed\Setup;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class UpgradeData implements UpgradeDataInterface
{

    protected $_objectManager;
    protected $_queue;
    protected $_resourceConnection;

    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Hunters\MultiFeed\Helper\Queue $queue,
        \Magento\Framework\App\ResourceConnection $resourceConnection
    )
    {
        $this->_queue = $queue;
        $this->_objectManager = $objectManager;
        $this->_resourceConnection = $resourceConnection;
    }

    /**
     * Upgrades data for a module
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.4') < 0) {
            $this->_upgradeTo_1_0_4($setup, $context);
        }

        $setup->endSetup();
    }

    private function _upgradeTo_1_0_4(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $connection = $this->_resourceConnection->getConnection();
        $tableSubscriptionsProfiles = $this->_resourceConnection->getTableName('subscriptions_profiles');
        $tableHuntersMultifeedQueue = $this->_resourceConnection->getTableName('hunters_multifeed_queue');
        $sql = "select profile_id, merchant_source, status from ".$tableSubscriptionsProfiles." where profile_id not in (select entity_id from ".$tableHuntersMultifeedQueue." where entity_type = 'active_subs') and status ='active';";
        $profiles = $connection->fetchAll($sql);

        foreach ($profiles as $profile) {
            if ($profile['status'] == 'active') {
                $this->_queue->add($profile['profile_id'], 'active_subs');
            }
        }
    }
}

