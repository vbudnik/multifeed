<?php

namespace Hunters\MultiFeed\Setup;


use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\DB\Ddl\Table;


class UpgradeSchema implements UpgradeSchemaInterface
{
  public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
  {
      $setup->startSetup();

      if(version_compare($context->getVersion(), '1.0.1') < 0) {
          $this->upgradeTo_1_0_1($setup, $context);
      }

      if(version_compare($context->getVersion(), '1.0.2') < 0) {
          $this->upgradeTo_1_0_2($setup, $context);
      }


  }

  public function upgradeTo_1_0_1(SchemaSetupInterface $setup, ModuleContextInterface $context)
  {
    $setup->getConnection()
        ->addColumn(
            $setup->getTable('hunters_multifeed_queue'),
            'store_id',
            'smallint(6) default null'
        );
  }

  public function upgradeTo_1_0_2(SchemaSetupInterface $setup, ModuleContextInterface $context)
  {
    $setup->getConnection()->addColumn(
      $setup->getTable('salesrule'),
      'sf_campaign_code',
      [
          'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
          'length' => 200,
          'nullable' => true,
          'default' => '',
          'comment' => 'SalesForce Campaign Code'
      ]
    );
  }
}
